#include "test_utils.h"

#include <string.h>

int
main() {
  printf("\n");
  LinkedList list =
    LIST_CREATE(sizeof(const char*), copyCStringAddress, NULL);

  const char* s1 = "Test123";
  const char* s2 = "Another";
  const char* s3 = "Pointer";

  printf("(%p)\ts1: %s\n", s1, s1);
  printf("(%p)\ts2: %s\n", s2, s2);
  printf("(%p)\ts3: %s\n", s3, s3);
  printf("\n");

  LinkedList_add(list, &s1);
  assert(0 == strcmp(s1, *((char**) LinkedList_elementAt(list, 0))));

  LIST_PRINT(list, forEachCString);
  printf("\n");

  LinkedList_add(list, &s2);
  LinkedList_add(list, &s3);
  assert(0 == strcmp(s2, *((char**) LinkedList_elementAt(list, 1))));
  assert(0 == strcmp(s3, *((char**) LinkedList_elementAt(list, 2))));

  LIST_PRINT(list, forEachCString);

  LinkedList_add(list, &s2);
  LinkedList_add(list, &s3);
  assert(0 == strcmp(s2, *((char**) LinkedList_elementAt(list, 3))));
  assert(0 == strcmp(s3, *((char**) LinkedList_elementAt(list, 4))));

  LIST_PRINT(list, forEachCString);

  LinkedList_destroy(list);

  return 0;
}