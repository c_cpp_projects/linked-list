include_directories(
  ${HOME_INC_DIR}
)

if(${LIB_LINKED_LIST_TEST_LOG})
  add_compile_definitions(
    LIB_LINKED_LIST_TEST_LOG=1    
  )
endif()

# Ignore certain warnings for tests
add_compile_options(
  -Wno-unused-parameter
  -Wno-unused-variable
)

# Use this function to add tests
#
# If you have the file "utils_test.c", then
# you can call 
# add_test_executable(utils_test)
function(add_test_executable TARGET)
  # Check if correct # of args was passed
  if(NOT ${ARGC} EQUAL 1)
    message(FATAL_ERROR "Call to 'add_test_executable' with ${ARGC} arguments instead of 1.")
  endif()

  add_executable(${TARGET} ${TARGET}.c)
  target_link_libraries(${TARGET}
    ${LIB_LINKED_LIST} pthread
  )

  install(
    TARGETS ${TARGET}
    DESTINATION ${HOME_BIN_DIR}/tests
  )

  add_test(
    NAME ${TARGET} 
    COMMAND ${TARGET}
  )

endfunction()

add_test_executable(linked_list_add_all_test)
add_test_executable(linked_list_add_array_test)
add_test_executable(linked_list_add_test)
add_test_executable(linked_list_binary_search_test)
add_test_executable(linked_list_concurrency_test)
add_test_executable(linked_list_contains_test)
add_test_executable(linked_list_copy_test)
add_test_executable(linked_list_cstring_test)
add_test_executable(linked_list_element_at_test)
add_test_executable(linked_list_first_where_test)
add_test_executable(linked_list_for_each_test)
add_test_executable(linked_list_insert_test)
add_test_executable(linked_list_last_where_test)
add_test_executable(linked_list_merge_sort_test)
add_test_executable(linked_list_next_and_previous_test)
add_test_executable(linked_list_put_test)
add_test_executable(linked_list_remove_at_2_test)
add_test_executable(linked_list_remove_at_test)
add_test_executable(linked_list_remove_first_where_test)
add_test_executable(linked_list_remove_last_where_test)
add_test_executable(linked_list_remove_where_test)
add_test_executable(linked_list_reverse_test)
add_test_executable(linked_list_sort_and_search_test)
add_test_executable(linked_list_to_array_test)
add_test_executable(linked_list_version_test)