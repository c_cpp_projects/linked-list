#include "test_utils.h"

int
main() {
  LinkedList list = LIST_CREATE(sizeof(TPerson), copyPerson, destroyPerson);

  TPerson pers1 = {4, "Peter"};
  TPerson pers2 = {7, "Sragrl"};
  TPerson pers3 = {10, "Norbert"};
  TPerson pers4 = {541, "Manfred"};
  TPerson pers5 = {91, "Hofer"};
  TPerson pers6 = {4441, "Lidl"};

  TPerson* key = &pers2;
  // TPerson* key = &pers6;
  // const TPerson* key = &pers6;

  LinkedList_add(list, &pers1);
  LinkedList_add(list, &pers2);
  LinkedList_add(list, &pers3);
  LinkedList_add(list, &pers4);

  TPerson found;
  memset(&found, 0, sizeof(TPerson));

  printf("before:\n");
  printTPerson(&found);
  printf("\n");

  bool contains = LinkedList_contains(list, &found, key, 0, containsTPerson);
  printf("Contains %s: %s\n\n", key->name, contains ? "true" : "false");
  assert(contains);

  printf("after:\n");
  printTPerson(&found);
  printf("\n");

  LinkedList_destroy(list);
  return 0;
}