#include "test_utils.h"

int
main() {
  LinkedList list =
    LIST_CREATE(sizeof(TPerson), copyPerson, destroyPerson);

  TPerson persons[] = {
    {-947, "Ermund"},
    {-544, "Martin"},
    {7444, "Hranov"},
    {423, "Peter"},
    {-124, "Manfred"},
    {441, "Norman"},
    {-534, "Siegfried"},
    {10, "Hias"},
  };

  LinkedList_addArray(list, persons, LENGTH_OF(persons));

  LIST_PRINT(list, forEachPerson);

  TPerson* current;
  size_t i;
  for (i = 0; i < LinkedList_size(list); ++i) {
    current = (TPerson*) LinkedList_elementAt(list, i);
    printTPerson(current);
    assert(0 == memcmp(current, &persons[i], sizeof(TPerson)));
  }
  printf("\n");

  LinkedList_destroy(list);
  return 0;
}