#include <assert.h>
#include <string.h>

int
main(void) {
  assert(0 == strcmp("2.0.2", LIB_LINKED_LIST_VERSION));
  assert(202 == LIB_LINKED_LIST_VERSION_CODE);
  assert(2 == LIB_LINKED_LIST_VERSION_MAJOR);
  assert(0 == LIB_LINKED_LIST_VERSION_MINOR);
  assert(2 == LIB_LINKED_LIST_VERSION_PATCH);
  return 0;
}