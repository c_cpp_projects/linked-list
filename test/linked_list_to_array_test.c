#include "test_utils.h"

int
main() {
  LinkedList myList =
    LIST_CREATE(sizeof(TPerson), copyPerson, destroyPerson);

  TPerson pers1 = {5, "Peter"};
  TPerson pers2 = {54, "Srar"};
  TPerson pers3 = {1, "Norbert"};

  LinkedList_add(myList, &pers1);
  LinkedList_add(myList, &pers2);
  LinkedList_add(myList, &pers3);

  TPerson arr[LinkedList_size(myList)];
  LinkedList_toArray(myList, arr);

  printf("%p\n", (void*) arr);

  for (size_t i = 0; i < LinkedList_size(myList); ++i) {
    printf("[%lu]\tid: %d\tname: %s\n", i, arr[i].id, arr[i].name);
  }

  LinkedList_destroy(myList);

  return 0;
}