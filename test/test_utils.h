#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <assert.h>
#include <threads.h>
#include <time.h>
#include <unistd.h>

#include "linked_list.h"

#define LIST_PRINT_USE_FOREACH 1
#define LIST_PRINT_USE_PRINT   0
#define QUIET_DESTROY          1
#define QUIET_REMOVE           0
#define QUIET_MUTEX            1


#if (0 != LINKED_LIST_INCLUDE_PRINT && 0 != LIST_PRINT_USE_FOREACH && \
     0 != LIST_PRINT_USE_PRINT)
#  define LIST_PRINT(list, forEach)                                   \
    do {                                                              \
      printf("+++++++++++++++++++++++++++++++++++++++++++++++++\n");  \
      LinkedList_forEach(list, NULL, forEach);                        \
      printf("\n");                                                   \
      LinkedList_print(list, stdout);                                 \
      printf("------------------------------------------------\n\n"); \
    } while (0)
#elif (0 != LIST_PRINT_USE_FOREACH)
#  define LIST_PRINT(list, forEach) LinkedList_forEach(list, NULL, forEach), printf("\n")
#elif (0 != LINKED_LIST_INCLUDE_PRINT && 0 != LIST_PRINT_USE_PRINT)
#  define LIST_PRINT(list, forEach) LinkedList_print(list, stdout), printf("\n")
#else
#  define LIST_PRINT(list, forEach)
#endif

#define STRINGIFY(x) #x
#define LENGTH_OF(x) (sizeof(x) / sizeof(x[0]))

typedef struct {
  int id;
  char name[30];
} TPerson;

// Function declarations
#if 1
void
printMutexInfo(const char* tag, void* pMutex);
void
testSleep(int64_t amount, const char* unit);



void
copyPerson(void* destination, const void* source, size_t size);
void
destroyPerson(void* element, size_t size);
void
forEachPerson(void* userData, size_t index, void* element);
void
forEachPerson2(void* userData, size_t index, void* element);
bool
containsTPerson(void* userData, const void* key, const void* element);
bool
whereTPerson(void* userData, size_t index, const void* element);
bool
removeWhereTPerson(void* userData, size_t index, const void* element);
int
sortTPerson(const void* a, const void* b);
int
searchTPerson(const void* a, const void* b);
int
compareTPerson(const void* a, const void* b);
void
printTPerson(const TPerson* person);

void
copyCStringAddress(void* destination, const void* source, size_t size);
void
forEachCString(void* userData, size_t index, void* element);
void
printCString(const char* string);

void
forEachCB(void* userData, size_t index, void* element);
#endif

const LinkedList_Allocator listAllocator = {
  .calloc = calloc,
  .free = free,
};

#if (0 != LINKED_LIST_ENABLE_THREAD_SAFETY)
void
printMutexInfo(const char* tag, void* pMutex) {
  int64_t clk = (int64_t) clock();
  printf(" -%s (thread: %lu) (clk: %ld)\n", tag, (uint64_t) thrd_current(), clk);
  // fflush(stdout);
}

int
listMutexInit(void** pMutex) {
  *pMutex = malloc(sizeof(mtx_t));

#  if (0 == QUIET_MUTEX)
  printMutexInfo("Init\t ", *pMutex);
#  endif

  mtx_init(*pMutex, mtx_timed | mtx_recursive);

  return 0;
}

int
listMutexLock(void* pMutex) {
#  if (0 == QUIET_MUTEX)
  printMutexInfo("Lock\t ", pMutex);
#  endif

  mtx_lock(pMutex);

  return 0;
}

int
listMutexUnlock(void* pMutex) {
#  if (0 == QUIET_MUTEX)
  printMutexInfo("Unlock ", pMutex);
#  endif

  mtx_unlock(pMutex);

  return 0;
}

int
listMutexDestroy(void* pMutex) {
#  if (0 == QUIET_MUTEX)
  printMutexInfo("Destroy", pMutex);
#  endif

  mtx_destroy(pMutex);
  free(pMutex);

  return 0;
}

const LinkedList_Mutex listMutexFunctions = {
  .init = listMutexInit,
  .lock = listMutexLock,
  .unlock = listMutexUnlock,
  .destroy = listMutexDestroy,
};

#  define LIST_CREATE(elementSize, copy, destroy) \
    LinkedList_create(elementSize, copy, destroy, listAllocator, listMutexFunctions)

#  define LIST_CLONE(list) LinkedList_clone(list, listAllocator, listMutexFunctions)

#else

#  define LIST_CREATE(elementSize, copy, destroy) \
    LinkedList_create(elementSize, copy, destroy, listAllocator)

#  define LIST_CLONE(list) LinkedList_clone(list, listAllocator)

#endif



void
forEachCB(void* userData, size_t index, void* element);

/**
 * Let the current thread sleep.
 *
 * unit may be:
 *  - "ns" (nanoseconds)
 *  - "us" (microseconds)
 *  - "ms" (milliseconds)
 *  - "s"  (seconds)
 */
void
testSleep(int64_t amount, const char* unit) {
  if (0 == strcmp("s", unit)) {
    sleep((unsigned int) amount);
    return;
  }

  int factor = 0;
  if (0 == strcmp("ns", unit)) {
    factor = 1;
  } else if (0 == strcmp("us", unit)) {
    factor = 1000;
  } else if (0 == strcmp("ms", unit)) {
    factor = 1000000;
  }

  if (0 == factor) {
    printf("'%s' does not support the time unit '%s'\n", __ASSERT_FUNCTION, unit);
    fflush(stdout);
    abort();
  }

  __syscall_slong_t nsec = amount * factor;
  struct timespec ts = {.tv_sec = 0, .tv_nsec = nsec};
  thrd_sleep(&ts, NULL);
}

void
copyPerson(void* destination, const void* source, size_t size) {
  TPerson* dst = (TPerson*) destination;
  const TPerson* src = (TPerson*) source;

  *dst = *src;
}

void
destroyPerson(void* element, size_t size) {
#if (0 == QUIET_DESTROY)
  printf("Destroying ");
  printTPerson(element);
#endif
}

void
forEachCB(void* userData, size_t index, void* element) {
  printf("[%lu]: %p\n", index, element);
}

void
forEachPerson(void* userData, size_t index, void* element) {
  printf("[%lu]: ", index);
  TPerson* pers = (TPerson*) element;
  printTPerson(pers);
}

void
forEachPerson2(void* userData, size_t index, void* element) {
  TPerson* pers = (TPerson*) element;

  // simulate some work...
  char buf[strlen(pers->name) + 1];
  buf[strlen(pers->name) + 1] = '\0';
  memcpy(buf, pers->name, strlen(pers->name));
}

void
forEachCString(void* userData, size_t index, void* element) {
  const char* s = *((char**) element);
  printf("[%lu]:\t", index);
  printCString(s);
}

bool
containsTPerson(void* userData, const void* key, const void* element) {
  if (((TPerson*) key)->id == ((TPerson*) element)->id) {
    *((TPerson*) userData) = *((TPerson*) element);
    return true;
  }

  return false;
}

bool
whereTPerson(void* userData, size_t index, const void* element) {
  if (4 == ((const TPerson*) element)->id)
    return true;

  if (4 == index)
    return true;

  return false;
}

bool
removeWhereTPerson(void* userData, size_t index, const void* element) {
  const TPerson* p = (const TPerson*) element;

#if (0 == QUIET_REMOVE)
  printf("[%lu]:", index);
  printTPerson(p);
#endif

  int minId = 0;
  if (NULL != userData) {
    minId = *((int*) userData);
  }

  if (p->id < minId) {
#if (0 == QUIET_REMOVE)
    printf("Remove:");
    printTPerson(p);
#endif
    return true;
  }

  return false;
}

bool
wherePersonIdGreaterThan(void* pUserData, size_t index, const void* element) {
  const TPerson* p = (const TPerson*) element;
  const int minId = *((int*) pUserData);
  return (p->id > minId);
}

bool
wherePersonIdSmallerThan(void* pUserData, size_t index, const void* element) {
  const TPerson* p = (const TPerson*) element;
  const int minId = *((int*) pUserData);
  return (p->id < minId);
}

int
sortTPerson(const void* a, const void* b) {
  const TPerson* pA = (const TPerson*) a;
  const TPerson* pB = (const TPerson*) b;

  return pA->id <= pB->id;
  // return pA->id > pB->id;
  // return 0 >= strcmp(pA->name, pB->name);
}

int
searchTPerson(const void* a, const void* b) {
  const TPerson* pA = (const TPerson*) a;
  const TPerson* pB = (const TPerson*) b;

  return pA->id - pB->id;
  // return 0 == strcmp(pA->name, pB->name);
}

int
compareTPerson(const void* a, const void* b) {
  return searchTPerson(a, b);
}

void
printTPerson(const TPerson* person) {
  if (NULL == person) {
    printf("id: NULL\tname: NULL\taddress: %p\n", NULL);
  } else {
    printf("id: %d\tname: %s\taddress: %p\n", person->id, person->name, (void*) person);
  }
}

void
copyCStringAddress(void* destination, const void* source, size_t size) {
  const char* s = *((const char**) source);
  const uintptr_t addr = (uintptr_t) s;
  memcpy(destination, &addr, size);
}

void
printCString(const char* string) {
  if (NULL == string) {
    printf("%p:\t%s\n", NULL, "");
  } else {
    printf("%p:\t%s\n", (void*) string, string);
  }
}
