#include "test_utils.h"

int
main() {
  TPerson pers1 = {5, "Heinrich"};
  TPerson pers2 = {123, "Daniel"};
  TPerson pers3 = {78, "Norman"};

  LinkedList persons =
    LIST_CREATE(sizeof(TPerson), copyPerson, destroyPerson);

  LinkedList_add(persons, &pers1);
  LinkedList_add(persons, &pers2);
  LinkedList_add(persons, &pers3);

  LinkedList_forEach(persons, NULL, forEachPerson);

  LinkedList_destroy(persons);

  return 0;
}