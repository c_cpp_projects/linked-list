#include "test_utils.h"

int
main() {
  LinkedList list = LIST_CREATE(sizeof(TPerson), copyPerson, destroyPerson);

  TPerson persons[] = {
    {-947, "Ermund"},
    {-544, "Martin"},
    {7444, "Hranov"},
    {441,  "Norman"},
    {10,   "Hias"  },
  };

  LinkedList_addArray(list, persons, LENGTH_OF(persons));

  printf("list:\n");
  LIST_PRINT(list, forEachPerson);

  int minId = 7000;
  TPerson* found;

  found = (TPerson*) LinkedList_lastWhere(list, &minId, 0, wherePersonIdGreaterThan);
  printf("(should NOT find) (start: %d) last where id greater than %d\n", 0, minId);
  printTPerson(found);
  printf("\n");
  assert(NULL == found);

  found = (TPerson*) LinkedList_lastWhere(list, &minId, 1, wherePersonIdGreaterThan);
  printf("(should NOT find) (start: %d) last where id greater than %d\n", 1, minId);
  printTPerson(found);
  printf("\n");
  assert(NULL == found);

  found = (TPerson*) LinkedList_lastWhere(list, &minId, 2, wherePersonIdGreaterThan);
  printf("(should find) (start: %d) last where id greater than %d\n", 2, minId);
  printTPerson(found);
  printf("\n");
  assert(NULL != found);

  found = (TPerson*) LinkedList_lastWhere(list, &minId, 3, wherePersonIdGreaterThan);
  printf("(should find) (start: %d) last where id greater than %d\n", 3, minId);
  printTPerson(found);
  printf("\n");
  assert(NULL != found);

  found = (TPerson*) LinkedList_lastWhere(list, &minId, 10, wherePersonIdGreaterThan);
  printf("(should NOT find) (start: %d) last where id greater than %d\n", 10, minId);
  printTPerson(found);
  printf("\n");
  assert(NULL == found);

  LinkedList_destroy(list);

  return 0;
}