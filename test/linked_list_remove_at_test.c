#include "test_utils.h"

int
main() {
  LinkedList list =
    LIST_CREATE(sizeof(TPerson), copyPerson, destroyPerson);

  TPerson persons[10] = {{423, "Peter"},
                         {124, "Manfred"},
                         {441, "Norman"},
                         {534, "Siegfried"},
                         {714, "Hans Peter"},
                         {-947, "Ermund"},
                         {-544, "Martin"},
                         {416, "Samuel"},
                         {7444, "Hranov"},
                         {10, "Hias"}};

  LinkedList_addArray(list, persons, LENGTH_OF(persons));

  printf("Before removing:\n");
  LIST_PRINT(list, forEachPerson);

  LinkedList_removeAt(list, 0, false);
  LinkedList_removeAt(list, 0, false);
  LinkedList_removeAt(list, 0, false);
  printf("\n");

  printf("After removing:\n");
  LIST_PRINT(list, forEachPerson);

  LinkedList_destroy(list);

  return 0;
}