#include "test_utils.h"

int
main() {
  LinkedList list = LIST_CREATE(
    sizeof(TPerson), copyPerson, destroyPerson);

  TPerson pers1 = {4, "Peter"};
  TPerson pers2 = {7, "Sragrl"};
  TPerson pers3 = {10, "Norbert"};
  TPerson pers4 = {541, "Manfred"};
  TPerson pers5 = {91, "Hofer"};
  TPerson pers6 = {4441, "Lidl"};

  LinkedList_add(list, &pers1);
  LinkedList_add(list, &pers2);
  LinkedList_add(list, &pers3);
  LinkedList_add(list, &pers4);
  LinkedList_add(list, &pers5);
  LinkedList_add(list, &pers6);

  printf("LinkedList_elementAt(0)->name: %s\n", ((TPerson*) LinkedList_elementAt(list, 0))->name);
  printf("LinkedList_elementAt(1)->name: %s\n", ((TPerson*) LinkedList_elementAt(list, 1))->name);
  printf("\n");

  LinkedList_put(list, 1, &pers3, false);

  printf("LinkedList_elementAt(0)->name: %s\n", ((TPerson*) LinkedList_elementAt(list, 0))->name);
  printf("LinkedList_elementAt(1)->name: %s\n", ((TPerson*) LinkedList_elementAt(list, 1))->name);
  printf("\n");

  LIST_PRINT(list, forEachPerson);

  TPerson removedPers;
  LinkedList_removeAt(list, 1, &removedPers);
  printf("REMOVED: ");
  printTPerson(&removedPers);
  printf("\n");

  LIST_PRINT(list, forEachPerson);

  // LinkedList_clear(list);

  LIST_PRINT(list, forEachPerson);
  printf("\n");

  LinkedList_destroy(list);

  return 0;
}
