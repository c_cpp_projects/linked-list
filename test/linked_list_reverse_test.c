#include "test_utils.h"

int
main() {
  LinkedList myList =
    LIST_CREATE(sizeof(TPerson), copyPerson, destroyPerson);

  TPerson persons[] = {
    {-947, "Ermund"},
    {-544, "Martin"},
    {7444, "Hranov"},
    {423, "Peter"},
    {-124, "Manfred"},
    {441, "Norman"},
    {-534, "Siegfried"},
    {10, "Hias"},
  };

  LinkedList_addArray(myList, persons, LENGTH_OF(persons));



  printf("Normal:\n");
  LIST_PRINT(myList, forEachPerson);



  LinkedList_reverse(myList);
  printf("Reversed:\n");
  LIST_PRINT(myList, forEachPerson);



  LinkedList_reverse(myList);
  printf("Againg Reversed:\n");
  LIST_PRINT(myList, forEachPerson);



  LinkedList_destroy(myList);

  return 0;
}