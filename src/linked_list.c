#include "linked_list.h"



#if (0 != LINKED_LIST_TEST_THREAD_SAFETY)
#  include <stdlib.h>
#  include <threads.h>
#  include <time.h>

#  define RANDOM_THREAD_SLEEP()                             \
    do {                                                    \
      __syscall_slong_t nanos = 800000 + rand() % 20000000; \
      struct timespec ts = {.tv_sec = 0, .tv_nsec = nanos}; \
      thrd_sleep(&ts, NULL);                                \
    } while (0)

#else

#  define RANDOM_THREAD_SLEEP()

#endif



#if (0 != LINKED_LIST_ENABLE_THREAD_SAFETY)
#  define LINKED_LIST_LOCK(list) list->mutex.lock(list->pMutex)
#  define LINKED_LIST_UNLOCK(list)      \
    do {                                \
      list->mutex.unlock(list->pMutex); \
      RANDOM_THREAD_SLEEP();            \
    } while (0)
#else
#  define LINKED_LIST_LOCK(list)
#  define LINKED_LIST_UNLOCK(list) RANDOM_THREAD_SLEEP()
#endif



#define LINKED_LIST_ASSERT(expr) assert(expr)

#define CAST(type, value) ((type) (value))
#define CAST_PTR(x)       CAST(void*, x)
#define CAST_CONST_PTR(x) CAST(const void*, x)

/**
 * @brief Convert a @c TNode* to a @c LinkedList_ElementPtr .
 *
 * @param m_pNode A pointer to a @c TNode .
 */
#define TO_LINKED_LIST_ELEMENT_PTR(m_pNode) \
  CAST(LinkedList_ElementPtr, CAST(uintptr_t, m_pNode) + sizeof(TNode))

/**
 * @brief Convert a @c LinkedList_ElementPtr to a @c TNode* .
 *
 * @param m_pElement A pointer to a @c LinkedList_ElementPtr .
 */
#define TO_TNODE_PTR(m_pElement) CAST(TNode*, CAST(uintptr_t, m_pElement) - sizeof(TNode))

/***************** Declaration of Private Typedefs *****************/

typedef struct Node TNode;

/***************** Definition of Private Structs *****************/

struct LinkedListStruct {
  // Pointer to the head TNode, the head does not store any data.
  TNode* head;

  // Pointer to the tail TNode (the last TNode that contains an element).
  TNode* tail;

  // Current size of this List.
  size_t size;

  size_t elementSize;

  LinkedList_CopyCb copy;

  LinkedList_DestroyCb destroy;

  LinkedList_Allocator allocator;

#if (0 != LINKED_LIST_ENABLE_THREAD_SAFETY)
  LinkedList_Mutex mutex;
  void* pMutex;
#endif
};

struct Node {
  TNode* next;
  TNode* previous;
};

/***************** Declaration of Private Functions *****************/

/**
 * @brief Allocate memory for a new @c TNode .
 *
 *        Does not check if @c list is @b NULL .
 */
static inline TNode*
m_callocTNode(const LinkedList list);

/**
 * @brief Remove the element from @c pNode .
 *
 *        Does not check if @c list is @b NULL .
 *
 *
 * @param removeTNode @b True if @c pNode should get removed from the chain
 *                    of nodes from @c list and therefore deallocated. Also
 *                    decrements the size of @c list if @c pNode gets removed.
 *
 * @param removedElementBuffer If @b NON-NULL , then copy the memory from the
 *                             removed element into @c removedElementBuffer ,
 *                             otherwise the removed element gets destroyed.
 *
 *                             If @c removedElementBuffer is smaller than the size of
 *                             an element in @c list , then you enter the domain of
 *                             undefined behaviour.
 */
static inline void
m_remove(LinkedList list, TNode* pNode, bool removeTNode, void* removedElementBuffer);

/**
 * @brief Remove a TNode from the chain of TNodes from a LinkedList.
 *        The node does not get freed from memory.
 *
 *        Does not check if @c list or @c node , or both are @b NULL .
 */
static inline void
m_unchainTNode(LinkedList list, TNode* node);

/**
 * @brief Remove all @c TNodes in @c list .
 *
 *        After this function the size of @c list is 0.
 *
 *        Uses the @c LinkedList_DestroyCb , if @b NON-NULL , of @c list
 *        to destroy the elements.
 *
 *        Does not check if @c list is @b NULL .
 */
static void
m_clear(LinkedList list);

static void
m_merge(LinkedList list, size_t left, size_t middle, size_t right,
        const LinkedList_CompareCb compare);

static void
m_mergeSort(LinkedList list, size_t left, size_t right,
            const LinkedList_CompareCb compare);

/**
 * @brief If @c destination is @b NULL, allocate a new @c TNode .
 *
 *        Does not check if @c list is @b NULL .
 *
 *
 * @param pElement A pointer to the element you want to store in @c destination .
 *
 * @param destroyOldElement If this flag is @b true and if @c destination and
 *                          the @c LinkedList_DestroyCb of @c list are @b NON-NULL ,
 *                          then use the @c LinkedList_DestroyCb to destroy the
 *                          element stored in @c destination .
 *
 *
 * @return @c destination with the data from @c pElement .
 */
static inline TNode*
m_setTNode(LinkedList list, TNode* destination, const void* pElement,
           bool destroyOldElement);

/**
 * @brief Get a @c TNode* to the node at @c index in @c list .
 *
 *        Does not check if @c list is @b NULL or if @c index is valid.
 */
static TNode*
m_getTNodeAt(const LinkedList list, size_t index);


/**
 * @brief Check if @c list is empty, without aquiring a mutex lock.
 */
static inline bool
m_isEmpty(const LinkedList list);


/**
 * @brief Same as @c LinkedList_firstWhere() , but without aquiring a
 *        mutex lock.
 *
 *        Does not check if @c list is @b NULL .
 */
static LinkedList_ElementPtr
m_firstWhere(const LinkedList list, void* pUserData, size_t start,
             const LinkedList_TestWhereCb test);

/**
 * @brief Same as @c LinkedList_lastWhere() , but without aquiring a
 *        mutex lock.
 *
 *        Does not check if @c list is @b NULL .
 */
static LinkedList_ElementPtr
m_lastWhere(const LinkedList list, void* pUserData, size_t start,
            const LinkedList_TestWhereCb test);

/***************** Definition of Public Functions *****************/

void
LinkedList_add(LinkedList list, const void* pElement) {
  if (NULL == list)
    return;

  LINKED_LIST_LOCK(list);

  ++list->size;

  TNode* next = m_setTNode(list, NULL, pElement, false);
  next->previous = list->tail;

  // Because we do not store an element in list->head, we do not
  // have to check if the head is NULL.

  list->tail->next = next;
  list->tail = next;

  LINKED_LIST_UNLOCK(list);
}

void
LinkedList_addAll(LinkedList destination, const LinkedList source) {
  if (NULL == destination || NULL == source)
    return;

  LINKED_LIST_LOCK(destination);
  LINKED_LIST_LOCK(source);

  // nothing to add.
  if (m_isEmpty(source)) {
    LINKED_LIST_UNLOCK(destination);
    LINKED_LIST_UNLOCK(source);
    return;
  }

  // Cant add source to destination, because source contains
  // elements of another size (therefore probably another type as well).
  if (destination->elementSize != source->elementSize) {
    LINKED_LIST_UNLOCK(destination);
    LINKED_LIST_UNLOCK(source);
    return;
  }

  TNode* sourceNext = source->head->next;
  TNode* destinationNext;

  for (; NULL != sourceNext; sourceNext = sourceNext->next) {
    destinationNext =
      m_setTNode(destination, NULL, TO_LINKED_LIST_ELEMENT_PTR(sourceNext), false);

    destinationNext->previous = destination->tail;

    destination->tail->next = destinationNext;
    destination->tail = destination->tail->next;
  }

  LINKED_LIST_UNLOCK(destination);
  LINKED_LIST_UNLOCK(source);
}

void
LinkedList_addArray(LinkedList list, const void* base, size_t length) {
  if (NULL == list || NULL == base)
    return;

  if (0 == length)
    return;

  LINKED_LIST_LOCK(list);

  list->size += length;

  TNode* next;
  const void* pElement;

  for (size_t i = 0; i < length; ++i) {
    pElement = CAST_PTR(CAST(uintptr_t, base) + i * list->elementSize);

    next = m_setTNode(list, NULL, pElement, false);
    next->previous = list->tail;

    list->tail->next = next;
    list->tail = list->tail->next;
  }

  LINKED_LIST_UNLOCK(list);
}

LinkedList_ElementPtr
LinkedList_bsearch(const LinkedList list, const void* key,
                   const LinkedList_CompareCb compare) {
  if (NULL == list)
    return NULL;

  if (NULL == compare)
    return NULL;

  LINKED_LIST_LOCK(list);

  if (m_isEmpty(list)) {
    LINKED_LIST_UNLOCK(list);
    return NULL;
  }

  size_t left = 0;
  size_t right = list->size - 1;
  size_t middle;
  int result;
  LinkedList_ElementPtr pElement;

  while (left <= right) {
    middle = left + (right - left) / 2;
    pElement = TO_LINKED_LIST_ELEMENT_PTR(m_getTNodeAt(list, middle));
    result = compare(key, pElement);

    // found key
    if (0 == result) {
      LINKED_LIST_UNLOCK(list);
      return pElement;
    }

    // if 0 < result then the desired element appears
    // in the upper half else the desired element
    // appears in the lower half
    if (0 < result) {
      left = middle + 1;
    } else {
      right = middle;
    }
  }

  LINKED_LIST_UNLOCK(list);

  return NULL;
}

void
LinkedList_clear(LinkedList list) {
  if (NULL == list)
    return;

  LINKED_LIST_LOCK(list);
  m_clear(list);
  LINKED_LIST_UNLOCK(list);
}

bool
LinkedList_contains(const LinkedList list, void* pUserData, const void* pKey,
                    size_t start, const LinkedList_TestContainsCb contains) {
  if (NULL == list)
    return false;

  if (NULL == contains)
    return false;

  LINKED_LIST_LOCK(list);

  if (m_isEmpty(list)) {
    LINKED_LIST_UNLOCK(list);
    return false;
  }

  if (start >= list->size) {
    LINKED_LIST_UNLOCK(list);
    return false;
  }

  TNode* current = m_getTNodeAt(list, start);

  while (1) {
    if (contains(pUserData, pKey, TO_LINKED_LIST_ELEMENT_PTR(current))) {
      LINKED_LIST_UNLOCK(list);
      return true;
    }

    if (NULL == current->next)
      break;

    current = current->next;
  }

  LINKED_LIST_UNLOCK(list);

  return false;
}

LinkedList
LinkedList_clone(const LinkedList source,
                 LinkedList_Allocator allocator
                   LINKED_LIST_THREAD_SAFETY_PARAM(LinkedList_Mutex, mutex)) {
  if (NULL == source)
    return NULL;

  LINKED_LIST_LOCK(source);

  LinkedList copiedList =
    LinkedList_create(source->elementSize,
                      source->copy,
                      source->destroy,
                      allocator LINKED_LIST_THREAD_SAFETY_ARG(mutex));
  copiedList->size = source->size;

  TNode* current = source->head->next;
  TNode* next;

  // source->copy cannot be NULL, see LinkedList_create.
  for (; NULL != current; current = current->next) {
    const LinkedList_ElementPtr pElement = TO_LINKED_LIST_ELEMENT_PTR(current);
    next = m_setTNode(copiedList, NULL, pElement, false);

    copiedList->tail->next = next;
    next->previous = copiedList->tail;
    copiedList->tail = next;
  }

  LINKED_LIST_UNLOCK(source);

  return copiedList;
}

LinkedList
LinkedList_create(size_t elementSize, const LinkedList_CopyCb copy,
                  const LinkedList_DestroyCb destroy,
                  LinkedList_Allocator allocator
                    LINKED_LIST_THREAD_SAFETY_PARAM(LinkedList_Mutex, mutex)) {
  if (NULL == copy)
    return NULL;

  LinkedList list = (LinkedList) allocator.calloc(1, sizeof(struct LinkedListStruct));

  list->allocator = allocator;
  list->elementSize = elementSize;
  list->copy = copy;
  list->destroy = destroy;

#if (0 != LINKED_LIST_ENABLE_THREAD_SAFETY)
  list->mutex = mutex;
  list->mutex.init(&list->pMutex);
#endif

  // The head is a special TNode, because it does not store
  // an element, as for why this is the case, look into the
  // various LinkedList_add functions.
  //
  // Therefore we do not use m_callocTNode.
  list->head = CAST(TNode*, list->allocator.calloc(1, sizeof(TNode)));

  list->tail = list->head;

  return list;
}

void
LinkedList_destroy(LinkedList list) {
  if (NULL == list)
    return;

  LINKED_LIST_LOCK(list);
  m_clear(list);
  list->allocator.free(list->head);
  LINKED_LIST_UNLOCK(list);

#if (0 != LINKED_LIST_ENABLE_THREAD_SAFETY)
  list->mutex.destroy(list->pMutex);
#endif

  list->allocator.free(list);
}

LinkedList_ElementPtr
LinkedList_elementAt(const LinkedList list, size_t index) {
  if (NULL == list)
    return NULL;

  LINKED_LIST_LOCK(list);

  TNode* current = m_getTNodeAt(list, index);

  if (NULL == current) {
    LINKED_LIST_UNLOCK(list);
    return NULL;
  }

  LINKED_LIST_UNLOCK(list);
  return TO_LINKED_LIST_ELEMENT_PTR(current);
}

bool
LinkedList_equal(const LinkedList listA, const LinkedList listB,
                 const LinkedList_CompareCb compare, bool checkOrder) {
  if (NULL == compare)
    return false;

  // both point to the same address -> equal.
  if (listA == listB)
    return true;

  // One is NULL the other NON-NULL -> not equal.
  if (NULL == listA || NULL == listB)
    return false;

  LINKED_LIST_LOCK(listA);
  LINKED_LIST_LOCK(listB);

  // Different elementSize -> not equal.
  if (listA->elementSize != listB->elementSize) {
    LINKED_LIST_UNLOCK(listA);
    LINKED_LIST_UNLOCK(listB);
    return false;
  }

  if (checkOrder) {
    TNode* a = listA->head->next;
    TNode* b = listB->head->next;

    while (1) {
      // if NULL == a the NULL == b, because
      // both lists have the same length
      if (NULL == a)
        break;

      const int result =
        compare(TO_LINKED_LIST_ELEMENT_PTR(a), TO_LINKED_LIST_ELEMENT_PTR(b));
      if (0 != result) {
        LINKED_LIST_UNLOCK(listA);
        LINKED_LIST_UNLOCK(listB);
        return false;
      }

      a = a->next;
      b = b->next;
    }
  } else {
    size_t nodesIdx = 0;
    const size_t nodesSize = listB->size;
    const TNode* nodes[nodesSize];
    memset(nodes, 0, sizeof(TNode*) * nodesSize);

    for (TNode* a = listA->head->next; NULL != a; a = a->next) {
      // Check if listB contains currentA
      bool containsA = false;

      for (TNode* b = listB->head->next; NULL != b; b = b->next) {
        bool containsB = false;

        // if nodes already contains b, then that means that
        // there already was a node from listB that had the
        // same element as the one from a.
        for (size_t i = 0; i < nodesSize; ++i) {
          if (nodes[i] == b) {
            containsB = true;
            break;
          }
        }

        if (containsB)
          continue;

        const int result =
          compare(TO_LINKED_LIST_ELEMENT_PTR(a), TO_LINKED_LIST_ELEMENT_PTR(b));

        if (0 == result) {
          nodes[nodesIdx] = b;
          ++nodesIdx;
          containsA = true;
          break;
        }
      }

      if (!containsA) {
        LINKED_LIST_UNLOCK(listA);
        LINKED_LIST_UNLOCK(listB);
        return false;
      }
    }
  }

  LINKED_LIST_UNLOCK(listA);
  LINKED_LIST_UNLOCK(listB);

  return true;
}

bool
LinkedList_equalArray(const LinkedList list, const void* base, size_t length,
                      const LinkedList_CompareCb compare, bool checkOrder) {
  if (NULL == compare)
    return false;

  // both NULL -> equal
  if (NULL == list && NULL == base)
    return true;

  // one is NULL the other is NON-NULL -> not equal
  if ((NULL != list && NULL == base) || (NULL == list && NULL != base))
    return false;

  LINKED_LIST_LOCK(list);

  // Different length -> not equal.
  if (list->size != length) {
    LINKED_LIST_UNLOCK(list);
    return false;
  }

  if (checkOrder) {
    TNode* current = list->head->next;
    const void* pBaseElement = base;

    // We know that list and base have the same length.
    for (size_t i = 1; i < length; ++i) {
      const int result = compare(TO_LINKED_LIST_ELEMENT_PTR(current), pBaseElement);
      if (0 != result) {
        LINKED_LIST_UNLOCK(list);
        return false;
      }

      current = current->next;
      pBaseElement = CAST_PTR(CAST(uintptr_t, base) + i * list->elementSize);
    }
  } else {
    size_t nodesIdx = 0;
    const size_t nodesSize = list->size;
    const TNode* nodes[nodesSize];
    memset(nodes, 0, sizeof(TNode*) * nodesSize);

    const void* pBaseElement;

    for (size_t i = 0; i < length; ++i) {
      pBaseElement = CAST_PTR(CAST(uintptr_t, base) + i * list->elementSize);

      // Check if list contains the current pBaseElement.
      bool containsBaseElement = false;

      for (TNode* n = list->head->next; NULL != n; n = n->next) {
        bool containsNode = false;

        // if nodes already contains n, then that means that
        // there already was a node from list that matched with
        // an element from base and because we do not want to
        // use the same node from list for more than one match,
        // we skip it.
        for (size_t i = 0; i < nodesSize; ++i) {
          if (nodes[i] == n) {
            containsNode = true;
            break;
          }
        }

        if (containsNode)
          continue;

        const int result = compare(TO_LINKED_LIST_ELEMENT_PTR(n), pBaseElement);

        if (0 == result) {
          nodes[nodesIdx] = n;
          ++nodesIdx;
          containsBaseElement = true;
          break;
        }
      }

      if (!containsBaseElement) {
        LINKED_LIST_UNLOCK(list);
        return false;
      }
    }
  }

  LINKED_LIST_UNLOCK(list);

  return true;
}

LinkedList_ElementPtr
LinkedList_first(const LinkedList list) {
  if (NULL == list)
    return NULL;

  LINKED_LIST_LOCK(list);

  if (m_isEmpty(list)) {
    LINKED_LIST_UNLOCK(list);
    return NULL;
  }

  LinkedList_ElementPtr ptr = TO_LINKED_LIST_ELEMENT_PTR(list->head->next);

  LINKED_LIST_UNLOCK(list);

  return ptr;
}

LinkedList_ElementPtr
LinkedList_firstWhere(const LinkedList list, void* pUserData, size_t start,
                      const LinkedList_TestWhereCb test) {
  if (NULL == list)
    return NULL;

  LINKED_LIST_LOCK(list);
  // prevent optimization with volatile
  volatile LinkedList_ElementPtr ptr = m_firstWhere(list, pUserData, start, test);
  LINKED_LIST_UNLOCK(list);

  return CAST(LinkedList_ElementPtr, ptr);
}

void
LinkedList_forEach(const LinkedList list, void* userData,
                   const LinkedList_ForEachCb forEach) {
  if (NULL == list)
    return;

  if (NULL == forEach)
    return;

  LINKED_LIST_LOCK(list);

  if (m_isEmpty(list)) {
    LINKED_LIST_UNLOCK(list);
    return;
  }

  TNode* current = list->head->next;
  size_t index = 0;

  while (1) {
    forEach(userData, index, TO_LINKED_LIST_ELEMENT_PTR(current));

    if (NULL == current->next)
      break;

    ++index;
    current = current->next;
  }

  LINKED_LIST_UNLOCK(list);
}

void
LinkedList_insert(LinkedList list, size_t index, const void* pElement) {
  if (NULL == list)
    return;

  LINKED_LIST_LOCK(list);

  TNode* destination = m_getTNodeAt(list, index);
  if (NULL == destination) {
    LINKED_LIST_UNLOCK(list);
    return;
  }

  TNode* insert = m_setTNode(list, NULL, pElement, false);

  insert->next = destination;
  destination->previous->next = insert;

  insert->previous = destination->previous;
  destination->previous = insert;

  LINKED_LIST_UNLOCK(list);
}

bool
LinkedList_isEmpty(const LinkedList list) {
  if (NULL == list)
    return true;

  LINKED_LIST_LOCK(list);
  // prevent optimization with volatile
  volatile bool isEmpty = (0 == list->size);
  LINKED_LIST_UNLOCK(list);

  return CAST(bool, isEmpty);
}

bool
LinkedList_isNotEmpty(const LinkedList list) {
  if (NULL == list)
    return false;

  LINKED_LIST_LOCK(list);
  // prevent optimization with volatile
  volatile bool isNotEmpty = (0 != list->size);
  LINKED_LIST_UNLOCK(list);

  return CAST(bool, isNotEmpty);
}

LinkedList_ElementPtr
LinkedList_last(const LinkedList list) {
  if (NULL == list)
    return NULL;

  LINKED_LIST_LOCK(list);

  if (m_isEmpty(list)) {
    LINKED_LIST_UNLOCK(list);
    return NULL;
  }

  LinkedList_ElementPtr ptr = TO_LINKED_LIST_ELEMENT_PTR(list->tail);

  LINKED_LIST_UNLOCK(list);

  return ptr;
}

LinkedList_ElementPtr
LinkedList_lastWhere(const LinkedList list, void* pUserData, size_t start,
                     const LinkedList_TestWhereCb test) {
  if (NULL == list)
    return NULL;

  LINKED_LIST_LOCK(list);
  // prevent optimization with volatile
  volatile LinkedList_ElementPtr ptr = m_lastWhere(list, pUserData, start, test);
  LINKED_LIST_UNLOCK(list);

  return CAST(LinkedList_ElementPtr, ptr);
}

LinkedList_ElementPtr
LinkedList_next(LinkedList list, const LinkedList_ElementPtr pCurrentElement) {
  if (NULL == list)
    return NULL;

  if (NULL == pCurrentElement)
    return NULL;

  LINKED_LIST_LOCK(list);

  if (m_isEmpty(list)) {
    LINKED_LIST_UNLOCK(list);
    return NULL;
  }

  // If pCurrentElement is the last element return NULL.
  if (pCurrentElement == TO_LINKED_LIST_ELEMENT_PTR(list->tail)) {
    LINKED_LIST_UNLOCK(list);
    return NULL;
  }

  const TNode* current = TO_TNODE_PTR(pCurrentElement);
  if (NULL == current) {
    LINKED_LIST_UNLOCK(list);
    return NULL;
  }

  LINKED_LIST_UNLOCK(list);

  // We do not have to check if NULL == current->next, because
  // we already checked if pCurrentElement is the element from the
  // last TNode and only the last TNode has NULL as its
  // next TNode*.
  return TO_LINKED_LIST_ELEMENT_PTR(current->next);
}

LinkedList_ElementPtr
LinkedList_previous(LinkedList list, const LinkedList_ElementPtr pCurrentElement) {
  if (NULL == list)
    return NULL;

  if (NULL == pCurrentElement)
    return NULL;

  LINKED_LIST_LOCK(list);

  if (m_isEmpty(list)) {
    LINKED_LIST_UNLOCK(list);
    return NULL;
  }

  // If pCurrentElement is the first element return NULL.
  if (pCurrentElement == TO_LINKED_LIST_ELEMENT_PTR(list->head->next)) {
    LINKED_LIST_UNLOCK(list);
    return NULL;
  }

  TNode* current = TO_TNODE_PTR(pCurrentElement);
  if (NULL == current) {
    LINKED_LIST_UNLOCK(list);
    return NULL;
  }

  LINKED_LIST_UNLOCK(list);

  // We do not have to check if NULL == current->previous, because
  // we already checked if pCurrentElement is the element from the
  // first TNode and only the first TNode has NULL as its
  // previous TNode*.
  return TO_LINKED_LIST_ELEMENT_PTR(current->previous);
}

#if (0 != LINKED_LIST_INCLUDE_PRINT)
void
LinkedList_print(const LinkedList list, FILE* pFile) {
  if (NULL == list)
    return;

  LINKED_LIST_LOCK(list);

  if (NULL == pFile) {
    pFile = stdout;
  }

  fprintf(pFile,
          "HEAD:\tprevious: %p\t\t\tcurrent: %p\tnext: %p\n",
          CAST_PTR(list->head->previous),
          CAST_PTR(list->head),
          CAST_PTR(list->head->next));

  TNode* current = list->head->next;
  if (NULL == current) {
    LINKED_LIST_UNLOCK(list);
    return;
  }

  size_t index;
  for (index = 0; NULL != current->next; ++index) {
    fprintf(pFile,
            "[%lu]:\tprevious: %p\tcurrent: %p\tnext: %p\telement: %p\n",
            index,
            CAST_PTR(current->previous),
            CAST_PTR(current),
            CAST_PTR(current->next),
            CAST_PTR(TO_LINKED_LIST_ELEMENT_PTR(current)));
    current = current->next;
  }

  fprintf(pFile,
          "[%lu]:\tprevious: %p\tcurrent: %p\tnext: %p\t\telement: %p\n",
          index,
          CAST_PTR(current->previous),
          CAST_PTR(current),
          CAST_PTR(current->next),
          CAST_PTR(TO_LINKED_LIST_ELEMENT_PTR(current)));

  LINKED_LIST_UNLOCK(list);
}
#endif

bool
LinkedList_put(LinkedList list, size_t index, const void* pElement,
               void* oldElementbuffer) {
  if (NULL == list)
    return false;

  LINKED_LIST_LOCK(list);

  TNode* current = m_getTNodeAt(list, index);
  if (NULL == current) {
    LINKED_LIST_UNLOCK(list);
    return false;
  }

  m_remove(list, current, false, oldElementbuffer);

  // we already destroy the element in m_remove, so pass false.
  m_setTNode(list, current, pElement, false);

  LINKED_LIST_UNLOCK(list);

  return true;
}

bool
LinkedList_removeAt(LinkedList list, size_t index, void* removedElementBuffer) {
  if (NULL == list)
    return false;

  LINKED_LIST_LOCK(list);

  TNode* current = m_getTNodeAt(list, index);
  if (NULL == current) {
    LINKED_LIST_UNLOCK(list);
    return false;
  }

  m_remove(list, current, true, removedElementBuffer);

  LINKED_LIST_UNLOCK(list);

  return true;
}

bool
LinkedList_removeFirstWhere(LinkedList list, void* pUserData, size_t start,
                            const LinkedList_TestWhereCb test,
                            void* removedElementBuffer) {
  if (NULL == list)
    return false;

  LINKED_LIST_LOCK(list);

  LinkedList_ElementPtr pElement = m_firstWhere(list, pUserData, start, test);

  if (NULL != pElement) {
    m_remove(list, TO_TNODE_PTR(pElement), true, removedElementBuffer);
    LINKED_LIST_UNLOCK(list);
    return true;
  }

  LINKED_LIST_UNLOCK(list);

  return false;
}

bool
LinkedList_removeLastWhere(LinkedList list, void* pUserData, size_t start,
                           const LinkedList_TestWhereCb test,
                           void* removedElementBuffer) {
  if (NULL == list)
    return false;

  LINKED_LIST_LOCK(list);

  LinkedList_ElementPtr pElement = m_lastWhere(list, pUserData, start, test);

  if (NULL != pElement) {
    m_remove(list, TO_TNODE_PTR(pElement), true, removedElementBuffer);
    LINKED_LIST_UNLOCK(list);
    return true;
  }

  LINKED_LIST_UNLOCK(list);

  return false;
}

void
LinkedList_removeWhere(LinkedList list, void* userData,
                       const LinkedList_TestWhereCb test) {
  if (NULL == list)
    return;

  if (NULL == test)
    return;

  LINKED_LIST_LOCK(list);

  if (m_isEmpty(list)) {
    LINKED_LIST_UNLOCK(list);
    return;
  }

  TNode* current = list->head->next;
  TNode* previous;

  for (size_t i = 0; NULL != current; ++i, current = current->next) {
    if (!test(userData, i, CAST_CONST_PTR(TO_LINKED_LIST_ELEMENT_PTR(current))))
      continue;

    previous = current->previous;
    m_remove(list, current, true, NULL);
    current = previous;
  }

  LINKED_LIST_UNLOCK(list);
}

void
LinkedList_reverse(LinkedList list) {
  if (NULL == list)
    return;

  LINKED_LIST_LOCK(list);

  TNode* current = list->tail;
  TNode* previous = current->previous;
  TNode* next = list->head;

  list->head->next = list->tail;

  for (size_t i = 1; i < list->size; ++i) {
    current->next = previous;
    current->previous = next;

    next = current;
    current = previous;
    previous = current->previous;
  }

  current->next = NULL;
  current->previous = next;
  list->tail = current;

  LINKED_LIST_UNLOCK(list);
}

size_t
LinkedList_size(const LinkedList list) {
  if (NULL == list)
    return 0;

  LINKED_LIST_LOCK(list);
  volatile size_t size = list->size;
  LINKED_LIST_UNLOCK(list);

  return size;
}

void
LinkedList_sort(LinkedList list, const LinkedList_CompareCb compare) {
  if (NULL == list)
    return;

  if (NULL == compare)
    return;

  LINKED_LIST_LOCK(list);

  if (list->size <= 1) {
    LINKED_LIST_UNLOCK(list);
    return;
  }

  m_mergeSort(list, 0, list->size - 1, compare);

  LINKED_LIST_UNLOCK(list);
}

void
LinkedList_toArray(const LinkedList list, void* buffer) {
  if (NULL == list)
    return;

  LINKED_LIST_LOCK(list);

  if (m_isEmpty(list)) {
    LINKED_LIST_UNLOCK(list);
    return;
  }

  TNode* current = list->head->next;
  LinkedList_ElementPtr pElementDestination;

  for (size_t i = 0; NULL != current; ++i, current = current->next) {
    pElementDestination = CAST_PTR(CAST(uintptr_t, buffer) + i * list->elementSize);

    list->copy(pElementDestination,
               CAST_CONST_PTR(TO_LINKED_LIST_ELEMENT_PTR(current)),
               list->elementSize);
  }

  LINKED_LIST_UNLOCK(list);
}

/***************** Definition of Private Functions *****************/

static inline TNode*
m_callocTNode(const LinkedList list) {
  LINKED_LIST_ASSERT(NULL != list);
  void* p = list->allocator.calloc(1, sizeof(TNode) + list->elementSize);
  return CAST(TNode*, p);
}

static inline void
m_remove(LinkedList list, TNode* pNode, bool removeTNode, void* removedElementBuffer) {
  LINKED_LIST_ASSERT(NULL != list);

  if (removeTNode) {
    m_unchainTNode(list, pNode);
    --list->size;
  }

  if (NULL != removedElementBuffer) {
    list->copy(
      removedElementBuffer, TO_LINKED_LIST_ELEMENT_PTR(pNode), list->elementSize);
  } else if (NULL != list->destroy) {
    list->destroy(TO_LINKED_LIST_ELEMENT_PTR(pNode), list->elementSize);
  }

  if (removeTNode) {
    list->allocator.free(pNode);
  }
}

static inline void
m_unchainTNode(LinkedList list, TNode* node) {
  LINKED_LIST_ASSERT(NULL != list);
  LINKED_LIST_ASSERT(NULL != node);

  if (NULL != node->next) {
    node->next->previous = node->previous;
  }

  node->previous->next = node->next;

  if (list->tail == node) {
    list->tail = node->previous;
  }
}

static void
m_clear(LinkedList list) {
  LINKED_LIST_ASSERT(NULL != list);

  if (0 == list->size)
    return;

  TNode* last = list->head->next;
  TNode* current = last->next;

  for (; NULL != current; last = current, current = current->next) {
    if (NULL != list->destroy) {
      list->destroy(TO_LINKED_LIST_ELEMENT_PTR(last), list->elementSize);
    }

    list->allocator.free(last);
  }

  // The for loop always skips the tail (which is by now equal to last).
  if (NULL != list->destroy) {
    list->destroy(TO_LINKED_LIST_ELEMENT_PTR(last), list->elementSize);
  }
  list->allocator.free(last);

  list->head->next = NULL;
  list->tail = list->head;
  list->size = 0;
}

static void
m_merge(LinkedList list, size_t left, size_t middle, size_t right,
        const LinkedList_CompareCb compare) {
  // Use memcpy instead of list->copy for reordering the elements,
  // because normally there "should be no copies" when sorting the
  // elements in a list.

  size_t lenL = middle + 1 - left;
  size_t lenR = right - middle;

  uint8_t bufferL[lenL][list->elementSize];
  uint8_t bufferR[lenR][list->elementSize];

  size_t idxL;
  size_t idxR;
  TNode* current;

  current = m_getTNodeAt(list, left);
  for (idxL = 0; idxL < lenL; ++idxL, current = current->next) {
    memcpy(bufferL[idxL], TO_LINKED_LIST_ELEMENT_PTR(current), list->elementSize);
  }

  current = m_getTNodeAt(list, middle + 1);
  for (idxR = 0; idxR < lenR; ++idxR, current = current->next) {
    memcpy(bufferR[idxR], TO_LINKED_LIST_ELEMENT_PTR(current), list->elementSize);
  }

  // Compare and sort
  for (idxL = 0, idxR = 0, current = m_getTNodeAt(list, left); idxL < lenL && idxR < lenR;
       current = current->next) {
    const int result = compare(bufferL[idxL], bufferR[idxR]);

    if (0 != result) {
      memcpy(TO_LINKED_LIST_ELEMENT_PTR(current), bufferL[idxL], list->elementSize);
      ++idxL;
    } else {
      memcpy(TO_LINKED_LIST_ELEMENT_PTR(current), bufferR[idxR], list->elementSize);
      ++idxR;
    }
  }

  // Copy leftovers
  for (; idxL < lenL; ++idxL, current = current->next) {
    memcpy(TO_LINKED_LIST_ELEMENT_PTR(current), bufferL[idxL], list->elementSize);
  }

  for (; idxR < lenR; ++idxR, current = current->next) {
    memcpy(TO_LINKED_LIST_ELEMENT_PTR(current), bufferR[idxR], list->elementSize);
  }
}

static void
m_mergeSort(LinkedList list, size_t left, size_t right,
            const LinkedList_CompareCb compare) {
  if (left >= right)
    return;

  size_t middle = left + (right - left) / 2;

  // Call m_mergeSort on left half
  m_mergeSort(list, left, middle, compare);

  // Call m_mergeSort on right half
  m_mergeSort(list, middle + 1, right, compare);

  m_merge(list, left, middle, right, compare);
}

static inline TNode*
m_setTNode(LinkedList list, TNode* destination, const void* pElement,
           bool destroyOldElement) {
  LINKED_LIST_ASSERT(NULL != list);

  if (NULL == destination) {
    destination = m_callocTNode(list);
  } else if (!destroyOldElement && NULL != list->destroy) {
    list->destroy(TO_LINKED_LIST_ELEMENT_PTR(destination), list->elementSize);
  }

  list->copy(TO_LINKED_LIST_ELEMENT_PTR(destination), pElement, list->elementSize);

  return destination;
}

static TNode*
m_getTNodeAt(const LinkedList list, size_t index) {
  LINKED_LIST_ASSERT(NULL != list);

  TNode* current;

  if (index <= list->size / 2) {
    // start at head
    current = list->head->next;
    for (; NULL != current && index > 0; --index) {
      current = current->next;
    }
  } else {
    // start at tail
    current = list->tail;
    for (size_t i = 0; NULL != current && (list->size - 1) - index > i; ++i) {
      current = current->previous;
    }
  }

  return current;
}

static inline bool
m_isEmpty(const LinkedList list) {
  if (NULL == list)
    return true;

  return (0 == list->size);
}

static LinkedList_ElementPtr
m_firstWhere(const LinkedList list, void* pUserData, size_t start,
             const LinkedList_TestWhereCb test) {
  LINKED_LIST_ASSERT(NULL != list);

  if (NULL == test)
    return NULL;

  if (m_isEmpty(list))
    return false;

  if (start >= list->size)
    return false;

  TNode* current = list->head->next;

  for (size_t i = 0; i < start && NULL != current; ++i) {
    current = current->next;
  }

  if (NULL == current)
    return NULL;

  for (size_t i = start; NULL != current; ++i, current = current->next) {
    if (!test(pUserData, i, CAST_CONST_PTR(TO_LINKED_LIST_ELEMENT_PTR(current))))
      continue;

    return TO_LINKED_LIST_ELEMENT_PTR(current);
  }

  return NULL;
}

static LinkedList_ElementPtr
m_lastWhere(const LinkedList list, void* pUserData, size_t start,
            const LinkedList_TestWhereCb test) {
  LINKED_LIST_ASSERT(NULL != list);

  if (NULL == test)
    return NULL;

  if (m_isEmpty(list))
    return NULL;

  if (start >= list->size)
    return NULL;

  TNode* current = list->tail;

  for (size_t i = list->size - 1 - start; i > 0 && NULL != current; --i) {
    current = current->previous;
  }

  if (list->head == current)
    return NULL;

  for (size_t i = start; list->head != current; --i, current = current->previous) {
    if (!test(pUserData, i, CAST_CONST_PTR(TO_LINKED_LIST_ELEMENT_PTR(current))))
      continue;

    return TO_LINKED_LIST_ELEMENT_PTR(current);
  }

  return NULL;
}