# 2.0.2

  * Improved internal usage of mutex lock mechanism.
  
  * Removed some warnings for test files only.

# 2.0.1

  * Sorted library functions alphabetically.

# 2.0.0

  * Breaking changes: changed naming scheme of entire library. Every function/typedef/macro now has the prefix `LinkedList_`.
  
  * Reformatted comments in header file.

  * Added suffix `_test` to all tests.

  * Added test for concurrency.

# 1.2.1

  * Added the option to use a `LinkedList_Mutex`, so that all list functions are thread safe.

# 1.2.0

  * Bugfix in `LinkedList_removeWhere` and `LinkedList_removeAt`, where removing the last element of a list, made it impossible to add anymore elements to the list, but you could still retrieve pointers to non existent elements, which caused the program to crash.

  * Bugfix in `LinkedList_clone`, if the source list was a list that only holds addresses, then the `LinkedList_clone` method did not correctly copy those addresses. Therefore the returned copy of source did not contain the correct addresses, but rather random garbage.

  * Added `LinkedList_firstWhere`, `LinkedList_lastWhere`, `LinkedList_removeFirstWhere` and `LinkedList_removeLastWhere`, as well as tests for the new methods. 
  
  * Changed the way elements are stored, now there is no distinction between a pointer or a normal value, everything gets treated as a byte sequence.

  * Rewrote nearly all comments, increased understandability of all comments.

# 1.1.5

  * Removed the restriction on `LinkedList_create` for `copy` and `destroy` to be **NON-NULL**.

# 1.1.4

  * Changed the way data is stored internally in the linked list. This has no effect on the user.

# 1.1.3

  * Added `LinkedList_next` and `LinkedList_previous` functions.

  * Added **test/linked_list_next_and_previous.c** for testing `LinkedList_next` and `LinkedList_previous`.

# 1.1.2

  * Changed the projects structure.

# 1.1.1

  * Changed the implementation of LinkedList_bsearch, now using an iterative algorithm with a space complexity of O(1).

# 1.1.0

  * Changed implementation of the LinkedList, now if a LinkedList_CopyCb is provided then the LinkedList stores a copy of the added element instead of just the pointer to the element. You can still aquire the old behaivor, where only the pointer to the element was stored, by setting the LinkedList_CopyCb to NULL. 

  * Also implemented a DestroyCallback in case some extra resources have to be freed from the copied elements.

# 1.0.1

  * Fixed a bug in LinkedList_bsearch, where only elements before and inclusive the middle where found

# 1.0.0

  * Added LinkedList_bsearch 
  * Added test18 which tests the LinkedList_bsearch function
  * Added test19 which uses LinkedList_sort for sorting the LinkedList and then searches for key using LinkedList_bsearch.
